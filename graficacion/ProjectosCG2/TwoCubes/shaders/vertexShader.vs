#version 430 core

layout (location = 0) in vec4 vPosition;
layout (location = 1) in vec4 vColor;

out vec4 color;
uniform int time;
uniform mat4 transformations;

void main()
{
   color = vColor;
   vec4 newPos = vPosition;

   newPos = transformations * newPos;

   gl_Position = newPos;
}