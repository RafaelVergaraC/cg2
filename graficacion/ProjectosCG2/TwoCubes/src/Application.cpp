
#include "Application.h"
#include <iostream>
#include <vector>

#include "GL\glew.h"
#include <gl/GL.h>
#include <gl/GLU.h>

#include "ShaderFuncs.h"

#include "glm/common.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

GLfloat vertexPositions[] = {
	-1.0f, 1.0f, 1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, 1.0f, 1.0f,//t1

	-1.0f, 1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,//t2

	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, -1.0f, 1.0f,//t3

	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f,-1.0f, -1.0f, 1.0f,
	1.0f, 1.0f, -1.0f, 1.0f,//t4

	1.0f, 1.0f, -1.0f, 1.0f,
	1.0f,-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f, 1.0f,//t5

	1.0f, 1.0f, -1.0f, 1.0f,
	-1.0f,-1.0f, -1.0f, 1.0f,
	-1.0f, 1.0f, -1.0f, 1.0f,//t6

	-1.0f, 1.0f, -1.0f, 1.0f,
	-1.0f,-1.0f, -1.0f, 1.0f,
	-1.0f, -1.0f, 1.0f, 1.0f,//t7

	-1.0f, 1.0f, -1.0f, 1.0f,
	-1.0f,-1.0f, 1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, 1.0f,//t8

	-1.0f, 1.0f, -1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,//t9

	-1.0f, 1.0f, -1.0f, 1.0f,
	1.0f, 1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, -1.0f, 1.0f,//t10


	-1.0f, -1.0f, 1.0f, 1.0f,
	-1.0f, -1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, -1.0f, 1.0f,//t11

	-1.0f, -1.0f, 1.0f, 1.0f,
	1.0f, -1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f, 1.0f,//t12
};


GLfloat vertexColors[] = {
	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,

	0.0, 0.0, 1.0, 1.0,
	0.0, 0.0, 0.5, 1.0,
	0.0, 0.0, 1.0, 1.0,
};

Application::Application() : time(0), eye(3.1f, 0.1f, -18.1f), target(eye + glm::vec3(1.0f, 0.0f, 0.0f)), player(true), angle(0){

}

Application::~Application() 
{}

void Application::setup()
{
	std::string strVertexShader = loadTextFile("shaders/vertexShader.vs");
	std::string strFragmentShader = loadTextFile("shaders/fragmentShader.fs");
	InitializeProgram(triangle.shader, strVertexShader, strFragmentShader);
	timeId = glGetUniformLocation(triangle.shader, "time");
	transformationsId = glGetUniformLocation(triangle.shader, "transformations");
	triangle.numVertex = 36;
	glGenVertexArrays(1, &triangle.vao);
	glBindVertexArray(triangle.vao);
	glGenBuffers(1, &triangle.vbo);
	glBindBuffer(GL_ARRAY_BUFFER, triangle.vbo);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions), vertexPositions, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions)+sizeof(vertexColors), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0/*offset*/, sizeof(vertexPositions), vertexPositions);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertexPositions), sizeof(vertexColors), vertexColors);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)sizeof(vertexPositions));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnable(GL_DEPTH_TEST);
}

void Application::update()
{
	if (player) {
		playerViewUpdate();
	}
	else {
		orthoViewUpdate();
	}
}


void Application::display()
{
	if (player) {
		playerViewDisplay();
	}
	else {
		orthoViewDisplay();
	}
}

void Application::keyboard(int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		exit(0);
	}


	if (key == GLFW_KEY_P && action == GLFW_RELEASE) {
		player = !player;
	}

	if (key == GLFW_KEY_W && action == GLFW_RELEASE) {
		if (player) {
			glm::vec3 dist = target - eye;
			eye += dist;
			target += dist;
		}
	}
	else if (key == GLFW_KEY_S && action == GLFW_RELEASE) {
		if (player) {
			glm::vec3 dist = target - eye;
			eye -= dist;
			target -= dist;
		}
	}
	else if (key == GLFW_KEY_A && action == GLFW_RELEASE) {
		if (player) {
			angle += 6;
			target.x = eye.x + cos(glm::radians((float)angle));
			target.z = eye.z + sin(glm::radians((float)angle));
		}
	}
	else if (key == GLFW_KEY_D && action == GLFW_RELEASE) {
		if (player) {
			angle -= 6;
			target.x = eye.x + cos(glm::radians((float)angle));
			target.z = eye.z + sin(glm::radians((float)angle));
		}
	}
}

void Application::mousePosition(double xpos, double ypos)
{
	if (!player) {
		int x = 480 / 20;
		int y = 480 / 20;

		mousePosX = xpos / x;
		mousePosY = ypos / x;
	}
}

void Application::mouseButtonPressed(int button, int action, int mods) {
	if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
		if (!player) {
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 20; j++) {
					if (j == mousePosX && i == mousePosY) {
						if (map[mousePosY][mousePosX] >= 1) {
							map[mousePosY][mousePosX] = 0;
						}
						else {
							map[mousePosY][mousePosX]++;
						}
					}
				}
			}
		}
	}
}

void Application::playerViewUpdate()
{
	camera = glm::lookAt(eye, target, glm::vec3(0.0f, -1.0f, 0.0f));
	perspective = glm::perspective(45.0f, 1024.0f / 768.0f, 0.1f, 500.0f);

	transformations = glm::rotate(glm::mat4(1.0f), glm::radians((float)time), glm::vec3(1.0f, 0.0f, 0.0f));
	transformations = perspective * camera * transformations;
	
	glUniform1i(timeId, time);
	glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
}

void Application::playerViewDisplay()
{
	//Borramos el buffer de color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Seleccionamos los shaders a usar
	glUseProgram(triangle.shader);

	//activamos el vertex array object y dibujamos
	glBindVertexArray(triangle.vao);

	int j = 0;
	while (j < 20) {
		for (int i = 0; i < 20; i++) {
			if (map[j][i] == 1) {
				glDrawArrays(GL_TRIANGLES, 0, triangle.numVertex);
				transformations = glm::translate(transformations, glm::vec3(0.0f, 0.0f, -2.0f));
				glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
			}
			else {
				transformations = glm::translate(transformations, glm::vec3(0.0f, 0.0f, -2.0f));
				glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
			}
		}
		transformations = glm::translate(transformations, glm::vec3(2.0f, 0.0f, 40.0f));
		glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
		j++;
	}
}

void Application::orthoViewUpdate()
{
	secondCamera = glm::lookAt(glm::vec3(20.1f, 90.1f, 0.1f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	secondView = glm::ortho(-0.5f, 39.0f, 1.0f, -37.5f, 0.1f, 300.0f);

	transformations = glm::rotate(glm::mat4(1.0f), glm::radians((float)time), glm::vec3(1.0f, 0.0f, 0.0f));
	transformations = secondView * secondCamera * transformations;
	
	glUniform1i(timeId, time);
	glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));

	transformations = glm::translate(transformations, glm::vec3(38.0f, 0.0f, 0.0f));
	glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
}

void Application::orthoViewDisplay()
{
	//Borramos el buffer de color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Seleccionamos los shaders a usar
	glUseProgram(triangle.shader);

	//activamos el vertex array object y dibujamos
	glBindVertexArray(triangle.vao);

	int j = 0;
	while (j < 20) {
		for (int i = 0; i < 20; i++) {
			if (map[j][i] == 1) {
				glDrawArrays(GL_TRIANGLES, 0, triangle.numVertex);
				transformations = glm::translate(transformations, glm::vec3(0.0f, 0.0f, -2.0f));
				glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
			}
			else {
				transformations = glm::translate(transformations, glm::vec3(0.0f, 0.0f, -2.0f));
				glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
			}
		}
		transformations = glm::translate(transformations, glm::vec3(-2.0f, 0.0f, 40.0f));
		glUniformMatrix4fv(transformationsId, 1, GL_FALSE, glm::value_ptr(transformations));
		j++;
	}
}

void Application::reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
}